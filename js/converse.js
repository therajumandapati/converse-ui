(function() {

    var serverIp = "14.96.10.181";

    var participants;

    function Promise() {
        var successCb, failureCb;
        return {
            promise: {
                success: function(cb) {
                    successCb = cb;
                },
                failure: function(cb) {
                    failureCb = cb;
                }
            },
            resolve: function(data) {
                successCb(data);
            },
            reject: function(data) {
                failureCb(data);
            }
        }
    }

    var handle;

    function ConversationBuilder() {
        return {
            build: function(conversation) {
                return $('<div class="conversation">' +
                    '   <div class="conversation-title">' +
                    '       <h3>Temp User</h3>' +
                    '   </div>' +
                    '   <div class="conversation-body">' +
                    '       <p>' + conversation.text + '</p>' +
                    '   </div>' +
                    '</div>');
            }
        }
    }

    function Conversify() {
        var conversationsHolder = $('#conversationsHolder');
        var builder = ConversationBuilder();
        var conversationsMarkup = [];

        //AJAX
        $.ajax({
                method: 'GET',
                url: "http://" + serverIp + ":9000/api/conversation?userId=" + handle,
            })
            .done(function(conversations) {
                conversations.forEach(function(conversation) {
                    conversationsMarkup.push(builder.build(conversation));
                });

                conversationsHolder.append(conversationsMarkup);
            });
    }

    function Tokenizer() {
        return {
            tokenize: function(text) {
                var index = text.indexOf('@');
                if(index === -1) {
                  return [];
                } else {
                  var tempText = text.substr(index + 1, text.length);
                  if(tempText.indexOf(' ') === -1) return [];
                  var tokens = tempText.split(' ');
                  console.log(tokens);
                  tokens.length = 1;
                  return tokens;
                }
            }
        }
    }

    function SubmitConversation(conversation) {

        conversation.Participants = participants;
        if(handle && handle !== '') {
            conversation.Participants.push(handle);
        }

        $.ajax({
                method: 'POST',
                url: "http://" + serverIp + ":9000/api/conversation?userId=" + handle,
                data: conversation
            })
            .done(function(conversationId) {
                //do we need to receive the entire conversation and update the UI ???
            });
    }

    function conversationSubmit() {
        console.log('conversation submitted BITCH!');
    }

    function showNewConversationBox() {
        $('.conversation-new-enter').show();
    }

    function getPredictions(q) {

        var promise = new Promise();

        $.ajax({
                method: 'GET',
                url: "http://" + serverIp + ":9000/api/prediction",
                data: {
                    query: q
                }
            })
            .done(function(predictions) {
                promise.resolve(predictions);
            });

        return promise.promise;
    }

    function registerListeners() {
        $('#newConversation').click(function() {
            $('.conversation-placeholder').hide();
            showNewConversationBox();
        });

        $('#conversationSubmit').click(function() {
            var conversationText = $('textarea#newConversationEntry').val();
            SubmitConversation({
                text: conversationText
            });
        });

        $('textarea#newConversationEntry').bind('input propertychange', function(event) {
            var tokenizer = Tokenizer();
            var tokens = tokenizer.tokenize(event.target.value);
            if(tokens.length !== 0) {
              getPredictions(tokens[tokens.length - 1]).success(function(predictions) {
                  participants = [];
                  predictions.forEach(function(prediction) {
                    participants.push(prediction.id);
                  })
              });
            }
        });

        $('#registration-trigger').click(function() {
            $('#registration-form').show();
            $('.converse-parent').hide();
        });

        $('input#registration-handle').bind('input propertychange', function(event) {

            function createPredictionMarkup(prediction) {
                return '<div class="radio"><label>' +
                        '    <input type="radio"' +
                        '    name="user-prediction"' +
                        '    id="' + prediction.id + '"' +
                        '    value="' + prediction.id + '"' +
                        '>' +
                        prediction.fullName + '</label></div>';
            }

            getPredictions(event.target.value).success(function(predictions) {
                if(predictions) {
                    var predictionsMarkup = [];
                    predictions.forEach(function(prediction) {
                        predictionsMarkup.push(createPredictionMarkup(prediction));
                    });
                    $('#user-predictions-container').empty();
                    $('#user-predictions-container').append(predictionsMarkup);
                } else {
                    $('#user-predictions-container').empty();
                    return;
                }
            });
        });

        $('#registration-form-save').click(function() {
            var checkedPrediction = $('#user-predictions-container input[type="radio"]:checked:first').val();
            if (!checkedPrediction) {
                alert('please select a user');
                return;
            }
            handle = checkedPrediction;
            $('#registration-form').hide();
            $('.converse-parent').show();
        });

        $('#registration-form-cancel').click(function() {
            $('#registration-form').hide();
            $('.converse-parent').show();
        });

        $('#reloadConversations').click(function() {
            $('#conversationsHolder').empty();
            Conversify();
        });
    }

    $(document).ready(function() {
        Conversify();
        registerListeners();
    })
})();
